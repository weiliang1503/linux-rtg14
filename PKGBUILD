# Maintainer: David Runge <dvzrv@archlinux.org>
# Contributor:  Joakim Hernberg <jbh@alchemy.lu>

pkgbase=linux-rtg14
pkgver=6.4.rt6
_pkgver=${pkgver/.rt/-rt}
pkgrel=4
pkgdesc='Linux RT g14'
arch=(x86_64)
url="https://gitlab.archlinux.org/archlinux/packaging/upstream/linux-rt/-/commits/v${_pkgver}"
license=(GPL2)
makedepends=(
  bc
  cpio
  git
  graphviz
  imagemagick
  libelf
  pahole
  perl
  python-sphinx
  python-sphinx_rtd_theme
  tar
  texlive-latexextra
  xmlto
  xz
)
options=(!strip)
source=(
    $pkgbase::git+https://gitlab.archlinux.org/archlinux/packaging/upstream/linux-rt#tag=v${_pkgver}?signed
    config
    "choose-gcc-optimization.sh"
    
    "sys-kernel_arch-sources-g14_files-0004-5.17+--more-uarches-for-kernel.patch"::"https://raw.githubusercontent.com/graysky2/kernel_compiler_patch/master/more-uarches-for-kernel-5.17+.patch"

    0001-acpi-proc-idle-skip-dummy-wait.patch

    0001-HID-amd_sfh-Add-support-for-tablet-mode-switch-senso.patch
    #0019-HID-amd_sfh-Add-keyguard-for-ASUS-ROG-X13-tablet.patch
    0001-platform-x86-asus-wmi-Add-safety-checks-to-dgpu-egpu.patch
    #0001-Revert-perf-x86-intel-Fix-unchecked-MSR-access-error.patch
    #0024-V8-0-4-PCI-vmd-Enable-PCIe-ASPM-and-LTR-on-select-hardware.patch
    
    #0027-mt76_-mt7921_-Disable-powersave-features-by-default.patch

    #0001-Add-tablet-mode-to-GV310.patch
    #0001-Fix-battery-on-elan-device.patch
    #0000-Enable-PCIe-ASPM-and-LTR-on-select-hardware.patch
    #0000-PATCH-v5-00-16-x86-make-PAT-and-MTRR-independent-from-each-other.patch
    #0000-PATCH-v9-01-13-rcu-Fix-missing-nocb-gp-wake-on-rcu_barrier.patch
    #check
    #0001-HDR-testing.patch
    #0001-HID-amd_sfh-Add-support-for-tablet-mode-switch-senso.patch
    0001-Revert-PCI-Add-a-REBAR-size-quirk-for-Sapphire-RX-56.patch
    #0001-ga502-snd-test.patch
    0001-linux6.1.y-bore2.4.1.patch
    #0001-mm-support-POSIX_FADV_NOREUSE.patch NEEDS FIX
    #0001-mm-x86-arm64-add-arch_has_hw_pte_young.patch
    #0001-sched-core-2022-12-12-sched-Clear-ttwu_pending-after-enqueue_task.patch
    #0001-thermal-6.2-rc1-Thermal-control-updates.patch
    #0001-x86-bugs-Flush-IBP-in-ib_prctl_set.patch
    #0001-x86-intel_epb-Set-Alder-Lake-N-and-Raptor-Lake-P-normal-EPB.patch
    #0001-x86_core_for_v6.2_Add-the-call-depth-tracking-mitigation-for-Retbleed-which-has.patch
    #0001-x86_mm_for_6.2_v2_Randomize-the-per-cpu-entry-areas.patch
    #0001-x86_sgx_for_6.2-Introduce-SGX-feature-Asynchrounous-Exit-Notification.patch
    
    #Fix
    #0001-acpica_allow_adress_space_handler.patch
    #0002-acpi-6.2-rc1-2-More-ACPI-updates.patch
    
    #0002-mm-add-vma_has_recency.patch
    #0002-mm-x86-add-CONFIG_ARCH_HAS_NONLEAF_PMD_YOUNG.patch
    #0003-mm-vmscan.c-refactor-shrink_node.patch
    #0003-thermal-6.2-rc3-Thermal-control-fix.patch
    #0004-Revert-include-linux-mm_inline.h-fold-__update_lru_s.patch
    #0005-ACPI-x86-s2idle-Add-a-quirk-for-ASUS-ROG-Zephyrus-G1.patch Not need?
    #0005-mm-multi-gen-LRU-groundwork.patch
    #0006-ACPI-x86-s2idle-Add-a-quirk-for-Lenovo-Slim-7-Pro-14.patch
    #0006-mm-multi-gen-LRU-minimal-implementation.patch
    #0007-ACPI-x86-s2idle-Add-a-quirk-for-ASUSTeK-COMPUTER-INC.patch
    #0007-mm-multi-gen-LRU-exploit-locality-in-rmap.patch
    #0008-mm-multi-gen-LRU-support-page-table-walks.patch
    #0009-ACPI-x86-s2idle-Add-another-ID-to-s2idle_dmi_table.patch
    #0009-mm-multi-gen-LRU-optimize-multiple-memcgs.patch
    #0010-mm-multi-gen-LRU-kill-switch.patch
    #0011-mm-multi-gen-LRU-thrashing-prevention.patch
    #0012-mm-multi-gen-LRU-debugfs-interface.patch
    #0013-mm-multi-gen-LRU-admin-guide.patch
    #0014-mm-multi-gen-LRU-design-doc.patch

    "sys-kernel_arch-sources-g14_files-0047-asus-nb-wmi-Add-tablet_mode_sw-lid-flip.patch"
    "sys-kernel_arch-sources-g14_files-0048-asus-nb-wmi-fix-tablet_mode_sw_int.patch"
    "sys-kernel_arch-sources-g14_files-0049-ALSA-hda-realtek-Add-quirk-for-ASUS-M16-GU603H.patch"
)
sha256sums=('SKIP'
            'fa8aeb79fd5293946677f48f78eb8e24816e92b67d38a26a99917d34a3996d21'
            '278118011d7a2eeca9971ac97b31bf0c55ab55e99c662ab9ae4717b55819c9a2'
            '81ad663925a0aa5b5332a69bae7227393664bb81ee2e57a283e7f16e9ff75efe'
            '0a7ea482fe20c403788d290826cec42fe395e5a6eab07b88845f8b9a9829998d'
            'b9a96e744d8dbcb9568afc66fa679723d22d8f2ed4ccc54ad5f9ce1e30351d03'
            '172dbc88d0a3cda78387f3c907fa4953c71cb1cb162f0b34f78b8b78924bc3d4'
            '7b16fce20b03babc9e149030f43e283534835bbd8835ba0a794fd0205fea1708'
            'cf30463dc8cefb00a73814e84f6c120c6a3d6dfe5e2476ad47b80a5c319a1d47'
            '15e912a66e4bbce1cf0450f1dc6610653df29df8dd6d5426f9c1b039490436c8'
            '444f2d86de8c2177655b01596f939f99c2e7abfa8efad8a509e0a334f42dfa85'
            '982a31e47d3d586789e1b3cdda25f75e3b71d810e7494202089b8f2cef7c0ef9')


export KBUILD_BUILD_HOST=archlinux
export KBUILD_BUILD_USER=$pkgbase
export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

prepare() {
  cd $pkgbase

  echo "Setting version..."
  echo "-$pkgrel" > localversion.10-pkgrel
  echo "${pkgbase#linux}" > localversion.20-pkgname

  local src
  for src in "${source[@]}"; do
      src="${src%%::*}"
      src="${src##*/}"
      # picking up the RT patch
      src="${src//patch.xz/patch}"
      [[ $src = *.patch ]] || continue
      echo "Applying patch $src..."
      patch -Np1 < "../$src"
  done

  # local src
  # for src in "${source[@]}"; do
  #     src="${src%%::*}"
  #     src="${src##*/}"
  #     [[ $src = *.patch ]] || continue
  #     msg2 "Applying patch $src..."
  #     patch -Np1 < "../$src"
  # done

  echo "Setting config..."
  cp ../config .config
  make olddefconfig
  # make nconfig

  make LSMOD=$HOME/.config/modprobed.db localmodconfig

  sh ${srcdir}/choose-gcc-optimization.sh 15
  
  make -s kernelrelease > version
  echo "Prepared $pkgbase version $(<version)"
}

build() {
  cd $pkgbase
  make all
}

_package() {
  pkgdesc="The $pkgdesc kernel and modules"
  depends=(coreutils initramfs kmod)
  optdepends=('wireless-regdb: to set the correct wireless channels of your country'
              'linux-firmware: firmware images needed for some devices')
  provides=(VIRTUALBOX-GUEST-MODULES WIREGUARD-MODULE KSMBD-MODULE)

  cd $pkgbase
  local kernver="$(<version)"
  local modulesdir="$pkgdir/usr/lib/modules/$kernver"

  echo "Installing boot image..."
  # systemd expects to find the kernel here to allow hibernation
  # https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
  install -Dm644 "$(make -s image_name)" "$modulesdir/vmlinuz"

  # Used by mkinitcpio to name the kernel
  echo "$pkgbase" | install -Dm644 /dev/stdin "$modulesdir/pkgbase"

  echo "Installing modules..."
  make INSTALL_MOD_PATH="$pkgdir/usr" INSTALL_MOD_STRIP=1 \
    DEPMOD=/doesnt/exist modules_install  # Suppress depmod

  # remove build and source links
  rm "$modulesdir"/{source,build}
}

_package-headers() {
  pkgdesc="Headers and scripts for building modules for the $pkgdesc kernel"
  depends=(pahole)

  cd $pkgbase
  local builddir="$pkgdir/usr/lib/modules/$(<version)/build"

  echo "Installing build files..."
  install -Dt "$builddir" -m644 .config Makefile Module.symvers System.map \
    localversion.* version vmlinux
  install -Dt "$builddir/kernel" -m644 kernel/Makefile
  install -Dt "$builddir/arch/x86" -m644 arch/x86/Makefile
  cp -t "$builddir" -a scripts

  # required when STACK_VALIDATION is enabled
  install -Dt "$builddir/tools/objtool" tools/objtool/objtool

  # required when DEBUG_INFO_BTF_MODULES is enabled
  install -Dt "$builddir/tools/bpf/resolve_btfids" tools/bpf/resolve_btfids/resolve_btfids

  echo "Installing headers..."
  cp -t "$builddir" -a include
  cp -t "$builddir/arch/x86" -a arch/x86/include
  install -Dt "$builddir/arch/x86/kernel" -m644 arch/x86/kernel/asm-offsets.s

  install -Dt "$builddir/drivers/md" -m644 drivers/md/*.h
  install -Dt "$builddir/net/mac80211" -m644 net/mac80211/*.h

  # https://bugs.archlinux.org/task/13146
  install -Dt "$builddir/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

  # https://bugs.archlinux.org/task/20402
  install -Dt "$builddir/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
  install -Dt "$builddir/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
  install -Dt "$builddir/drivers/media/tuners" -m644 drivers/media/tuners/*.h

  # https://bugs.archlinux.org/task/71392
  install -Dt "$builddir/drivers/iio/common/hid-sensors" -m644 drivers/iio/common/hid-sensors/*.h

  echo "Installing KConfig files..."
  find . -name 'Kconfig*' -exec install -Dm644 {} "$builddir/{}" \;

  echo "Removing unneeded architectures..."
  local arch
  for arch in "$builddir"/arch/*/; do
    [[ $arch = */x86/ ]] && continue
    echo "Removing $(basename "$arch")"
    rm -r "$arch"
  done

  echo "Removing documentation..."
  rm -r "$builddir/Documentation"

  echo "Removing broken symlinks..."
  find -L "$builddir" -type l -printf 'Removing %P\n' -delete

  echo "Removing loose objects..."
  find "$builddir" -type f -name '*.o' -printf 'Removing %P\n' -delete

  echo "Stripping build tools..."
  local file
  while read -rd '' file; do
    case "$(file -Sbi "$file")" in
      application/x-sharedlib\;*)      # Libraries (.so)
        strip -v $STRIP_SHARED "$file" ;;
      application/x-archive\;*)        # Libraries (.a)
        strip -v $STRIP_STATIC "$file" ;;
      application/x-executable\;*)     # Binaries
        strip -v $STRIP_BINARIES "$file" ;;
      application/x-pie-executable\;*) # Relocatable binaries
        strip -v $STRIP_SHARED "$file" ;;
    esac
  done < <(find "$builddir" -type f -perm -u+x ! -name vmlinux -print0)

  echo "Stripping vmlinux..."
  strip -v $STRIP_STATIC "$builddir/vmlinux"

  echo "Adding symlink..."
  mkdir -p "$pkgdir/usr/src"
  ln -sr "$builddir" "$pkgdir/usr/src/$pkgbase"
}

_package-docs() {
  pkgdesc="Documentation for the $pkgdesc kernel"

  cd $pkgbase
  local builddir="$pkgdir/usr/lib/modules/$(<version)/build"

  echo "Installing documentation..."
  local src dst
  while read -rd '' src; do
    dst="${src#Documentation/}"
    dst="$builddir/Documentation/${dst#output/}"
    install -Dm644 "$src" "$dst"
  done < <(find Documentation -name '.*' -prune -o ! -type d -print0)

  echo "Adding symlink..."
  mkdir -p "$pkgdir/usr/share/doc"
  ln -sr "$builddir/Documentation" "$pkgdir/usr/share/doc/$pkgbase"
}

pkgname=("$pkgbase" "$pkgbase-headers")
for _p in "${pkgname[@]}"; do
  eval "package_$_p() {
    $(declare -f "_package${_p#$pkgbase}")
    _package${_p#$pkgbase}
  }"
done

# vim:set ts=8 sts=2 sw=2 et:
